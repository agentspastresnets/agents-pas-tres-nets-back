package com.agentspastresnets.userms.enums;

    public enum UserRole {
        ADMIN,
        USER,
        NONE
    }

