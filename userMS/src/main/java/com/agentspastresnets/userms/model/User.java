package com.agentspastresnets.userms.model;

import com.agentspastresnets.userms.enums.UserRole;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="users")
public class User {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private int id;

    @Column(unique = true)
    private String name;

    private String password;
    private String picture;
    private String mail;
    private String title;

    @ManyToOne(cascade={CascadeType.ALL})
    @JoinColumn(name="friend_id")
    private User friend;

    @OneToMany(mappedBy="friend")
    private Set<User> friendList;

    @Enumerated(EnumType.STRING)
    @JsonProperty("role")
    private UserRole userRole;

    public User() {
        this.name = "";
        this.password = "";
        this.picture = "";
        this.mail = "";
        this.title = "";
        this.friendList = new HashSet<>();
        this.userRole = UserRole.NONE;
    }

    public User(int id, String name, String password, String picture, String mail, String title, UserRole userRole) {
        this.id = id;
        this.name = name;
        this.password = password;
        this.picture = picture;
        this.mail = mail;
        this.title = title;
        this.userRole = userRole;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    @JsonIgnore
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    public String getPicture() {
        return picture;
    }
    public void setPicture(String picture) {
        this.picture = picture;
    }
    public String getMail() {
        return mail;
    }
    public void setMail(String mail) {
        this.mail = mail;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public Set<User> getFriendList() {
        return friendList;
    }
    public void setFriendList(Set<User> friendList) {
        this.friendList = friendList;
    }

    public UserRole getUserRole()
    {
        return userRole;
    }
    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }
}
