package com.agentspastresnets.userms.dto;

import com.agentspastresnets.userms.enums.UserRole;

import javax.validation.constraints.NotNull;

public class UserDTO {
    @NotNull
    private String name;
    @NotNull
    private String password;

    private String mail;
    private UserRole role;

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public String getMail() {
        return mail;
    }
    public void setMail(String mail) {
        this.mail = mail;
    }
    public UserRole getRole() {
        return role;
    }
    public void setRole(UserRole role) {
        this.role = role;
    }
}
