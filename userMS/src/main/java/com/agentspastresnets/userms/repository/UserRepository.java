package com.agentspastresnets.userms.repository;

import com.agentspastresnets.userms.model.User;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {

    @Query("SELECT u FROM User u WHERE u.mail = :mail and u.password = :password")
    Optional<User> findByMailAndPassword(@Param("mail") String mail, @Param("password") String password);
}
