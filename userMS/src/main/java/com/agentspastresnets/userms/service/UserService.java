package com.agentspastresnets.userms.service;

import com.agentspastresnets.userms.dto.UserDTO;
import com.agentspastresnets.userms.enums.UserRole;
import com.agentspastresnets.userms.model.User;

import com.agentspastresnets.userms.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public Optional<User> getLoggedUser(String mail, String password){
        return userRepository.findByMailAndPassword(mail, password);
    }

    public Optional<User> getUser(int id) {
        return userRepository.findById(id);
    }

    public User addUser(UserDTO userDTO) {
        User newUser = new User();
        newUser.setName(userDTO.getName());
        newUser.setPassword(userDTO.getPassword());
        newUser.setMail(userDTO.getMail());
        //TODO - revoir si on donne none ou user
        newUser.setUserRole(UserRole.USER);
        userRepository.save(newUser);
        return newUser;
    }

    public void deleteUser(int id) {
        userRepository.deleteById(id);
    }

    public User updateUser(int id, UserDTO userDTO) {
        //get actual user
        Optional<User> userOpt = userRepository.findById(id);

        User userInit = new User();

        //put its information to the new user object
        if (userOpt.isPresent())
            userInit = userOpt.get();

        //get new username
        if (userDTO.getName()!=null && !userDTO.getName().isEmpty())
            userInit.setName(userDTO.getName());

        //get new password
        if (userDTO.getPassword()!=null && !userDTO.getPassword().isEmpty())
            userInit.setPassword(userDTO.getPassword());

        //get new mail
        if (userDTO.getMail()!=null && !userDTO.getMail().isEmpty())
            userInit.setMail(userDTO.getMail());

        //TODO update role
        //get new role
        if (userDTO.getRole()!=null )
            userInit.setUserRole(userDTO.getRole());

        return userRepository.save(userInit);
    }

}
