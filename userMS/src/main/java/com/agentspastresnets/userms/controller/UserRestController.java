package com.agentspastresnets.userms.controller;

import com.agentspastresnets.userms.dto.UserDTO;
import com.agentspastresnets.userms.model.User;
import com.agentspastresnets.userms.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/users")
public class UserRestController {

    private UserService userService;

    public UserRestController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/login")
    @ResponseBody
    public ResponseEntity getLoggedUser(@RequestParam("mail") String mail, @RequestParam("password") String password) {
        Optional<User> op = userService.getLoggedUser(mail, password);
        if (op.isPresent()) {
            return new ResponseEntity<>(op.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Wrong mail address or password.", HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/{id}")
    @ResponseBody
    public ResponseEntity getUser(@PathVariable int id) {
        Optional<User> op = userService.getUser(id);
        if (op.isPresent()) {
            return new ResponseEntity<>(op.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>("User with id " + id + " not found.", HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity addUser(@RequestBody UserDTO newUserDTO) {
        return new ResponseEntity<>(userService.addUser(newUserDTO), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @ResponseBody
    public ResponseEntity deleteUser(@PathVariable int id) {
        userService.deleteUser(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("/{id}")
    @ResponseBody
    public ResponseEntity updateUser (@PathVariable int id, @RequestBody UserDTO newUserDTO) {
        return new ResponseEntity<>(userService.updateUser(id, newUserDTO), HttpStatus.OK);
    }
}
