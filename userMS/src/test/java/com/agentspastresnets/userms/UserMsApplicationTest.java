package com.agentspastresnets.userms;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserMsApplicationTest {

    @Test
    public void contextLoads() {
    }

    @Test
    public void applicationContextTest() {
        UserMsApplication.main(new String[] {});
    }

}

