package com.agentspastresnets.userms.controller;

import com.agentspastresnets.userms.enums.UserRole;
import com.agentspastresnets.userms.model.User;
import com.agentspastresnets.userms.service.UserService;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(value = UserRestController.class , secure = false)
public class UserRestControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;

    @Test
    public void getLoggedUser() throws Exception {
        final User mockUser = new User(1, "john", "doe", "johnPic", "john.doe@cpe.fr", "Famous User", UserRole.USER);

        when(userService.getLoggedUser(Mockito.anyString(), Mockito.anyString())).thenReturn(java.util.Optional.of(mockUser));

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/users/login?mail=john.doe@cpe.fr&password=doe")
                .contentType(MediaType.APPLICATION_JSON);

        String result = mockMvc.perform(requestBuilder).andReturn().getResponse().getContentAsString();
        String expected = "{\"id\":1,\"name\":\"john\",\"picture\":\"johnPic\",\"mail\":\"john.doe@cpe.fr\",\"title\":\"Famous User\",\"role\":\"USER\"}";

        JSONAssert.assertEquals(expected, result, false);
    }

    @Test
    public void retrieveUserWithID() throws Exception {
        final User mockUser = new User(1, "john", "doe", "johnPic", "john.doe@cpe.fr", "Famous User", UserRole.USER);

        when(userService.getUser(Mockito.anyInt())).thenReturn(java.util.Optional.of(mockUser));

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/users/1")
                .contentType(MediaType.APPLICATION_JSON);

        String result = mockMvc.perform(requestBuilder).andReturn().getResponse().getContentAsString();
        String expected = "{\"id\":1,\"name\":\"john\",\"picture\":\"johnPic\",\"mail\":\"john.doe@cpe.fr\",\"title\":\"Famous User\",\"role\":\"USER\"}";

        JSONAssert.assertEquals(expected, result, false);
    }

    @Test
    public void addUser() throws Exception {
        User mockUser = new User();
        mockUser.setName("john");
        mockUser.setPassword("doe");

        when(userService.addUser(Mockito.any())).thenReturn(mockUser);

        String result = mockMvc.perform( post("/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(mockUser)))
                .andReturn().getResponse().getContentAsString();

        String expected = "{\"id\":0,\"name\":\"john\"}";

        JSONAssert.assertEquals(expected, result, false);
    }

    @Test
    public void deleteUser() throws Exception {
        this.mockMvc.perform( MockMvcRequestBuilders.delete("/users/1")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void updateUser() throws Exception {
        String content = "{\"name\":\"jane\",\"password\":\"doe\"}";
        this.mockMvc.perform( MockMvcRequestBuilders.put("/users/1")
                .content(content)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    // converts a Java object into JSON representation
    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}