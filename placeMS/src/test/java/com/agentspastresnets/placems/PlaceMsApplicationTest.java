package com.agentspastresnets.placems;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PlaceMsApplicationTest {

    @Test
    public void contextLoads() {
    }

    @Test
    public void applicationContextTest() {
        PlaceMsApplication.main(new String[] {});
    }

}

