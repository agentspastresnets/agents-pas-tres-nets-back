package com.agentspastresnets.placems.controller;

import com.agentspastresnets.placems.model.Place;
import com.agentspastresnets.placems.service.PlaceService;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONArray;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(value = PlaceRestController.class , secure = false)
public class PlaceRestControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PlaceService placeService;

    @Test
    public void getById() throws Exception{
        Place mockPlace = new Place("Hospital","Hospital_pic");
        mockPlace.setId(1);

        when(placeService.getPlace(Mockito.anyInt())).thenReturn(java.util.Optional.of(mockPlace));

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/places/1")
                .contentType(MediaType.APPLICATION_JSON);

        String result = mockMvc.perform(requestBuilder).andReturn().getResponse().getContentAsString();
        String expected = "{\"id\":1,\"name\":\"Hospital\",\"picture\":\"Hospital_pic\",\"usage\":0}";

        JSONAssert.assertEquals(expected, result, false);
    }

    @Test
    public void getAll() throws Exception{
        List<Place> mockPlaces = new ArrayList<>();
        mockPlaces.add(new Place("Hospital","Hospital_pic"));
        mockPlaces.add(new Place("Shop","Shop_pic"));

        when(placeService.getAll()).thenReturn(mockPlaces);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/places")
                .contentType(MediaType.APPLICATION_JSON);

        JSONArray result = new JSONArray();
        result.put(mockMvc.perform(requestBuilder).andReturn().getResponse().getContentAsString());
        JSONArray expected = new JSONArray();
        expected.put(
                "[{\"id\":0,\"name\":\"Hospital\",\"picture\":\"Hospital_pic\",\"usage\":0,\"metiers\":[]}," +
                "{\"id\":0,\"name\":\"Shop\",\"picture\":\"Shop_pic\",\"usage\":0,\"metiers\":[]}]");

        JSONAssert.assertEquals(result,expected,false);
    }

    @Test
    public void add() throws Exception{
        Place mockPlace = new Place("Hospital","Hospital_pic");
        mockPlace.setId(1);

        when(placeService.insertOrUpdate(Mockito.any(),  Mockito.anyBoolean())).thenReturn(mockPlace);

        String result = mockMvc.perform( post("/places")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(mockPlace)))
                .andReturn().getResponse().getContentAsString();

        String expected = "{\"id\":1,\"name\":\"Hospital\",\"picture\":\"Hospital_pic\",\"usage\":0}";

        JSONAssert.assertEquals(expected, result, false);
    }

    @Test
    public void update() throws Exception {

        Place mockPlace = new Place("Hospital","Hospital_pic");
        mockPlace.setId(1);

        when(placeService.insertOrUpdate(Mockito.any(),  Mockito.anyBoolean())).thenReturn(mockPlace);

        String content = "{\"name\":\"Asylum\",\"picture\":\"Asylum_pic\"}";
        this.mockMvc.perform( MockMvcRequestBuilders.put("/places/1")
                .content(content)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void remove() throws Exception{
        this.mockMvc.perform( MockMvcRequestBuilders.delete("/places/1")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    // converts a Java object into JSON representation
    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}