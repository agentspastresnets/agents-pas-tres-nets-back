package com.agentspastresnets.placems.repository;

import com.agentspastresnets.placems.model.Metier;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MetierRepository extends CrudRepository<Metier, Integer> {
    Optional<Metier> findByName(String name);
}
