package com.agentspastresnets.placems.repository;

import com.agentspastresnets.placems.model.Place;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PlaceRepository extends CrudRepository<Place, Integer> {
    Optional<Place> findByName(String name);
}
