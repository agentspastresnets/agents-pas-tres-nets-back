package com.agentspastresnets.placems.service;

import com.agentspastresnets.placems.model.Metier;
import com.agentspastresnets.placems.repository.MetierRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class MetierService {
    private final MetierRepository metierRepository;

    @Autowired
    public MetierService(MetierRepository metierRepository) {
        this.metierRepository = metierRepository;
    }

    public Optional<Metier> getMetier(Integer id){
        return metierRepository.findById(id);
    }

    public Optional<Metier> getMetier(String name) {
        return metierRepository.findByName(name);
    }

    public Metier insertOrUpdate(Metier m) {
        return metierRepository.save(m);
    }

    public Iterable<Metier> insertOrUpdate(Iterable<Metier> m) {
        return metierRepository.saveAll(m);
}
}
