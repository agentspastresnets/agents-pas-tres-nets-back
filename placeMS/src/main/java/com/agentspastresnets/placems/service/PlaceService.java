package com.agentspastresnets.placems.service;

import com.agentspastresnets.placems.model.Metier;
import com.agentspastresnets.placems.model.Place;
import com.agentspastresnets.placems.repository.MetierRepository;
import com.agentspastresnets.placems.repository.PlaceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class PlaceService {
    private final PlaceRepository placeRepository;
    private final MetierRepository metierRepository;

    @Autowired
    public PlaceService(PlaceRepository placeRepository, MetierRepository metierRepository) {
        this.placeRepository = placeRepository;
        this.metierRepository = metierRepository;
    }

    public Optional<Place> getPlace(int id) {
        return placeRepository.findById(id);
    }

    public Optional<Place> getPlace(String name) {
        return placeRepository.findByName(name);
    }

    public Iterable<Place> getAll() {
        return placeRepository.findAll();
    }

    public Place save(Place p) {
        return placeRepository.save(p);
    }

    public void remove(Integer id) {
        placeRepository.deleteById(id);
    }

    public Place insertOrUpdate(Place p, boolean insert) {
        //Management of the many to many association (parallel update on tables place and metier)
        Optional<Metier> om;
        Optional<Place> op;
        Set<Metier> toAdd = new HashSet<>();
        Set<Metier> toRem = new HashSet<>();
        Set<Metier> pSave = new HashSet<>(p.getMetiers());
        if (insert) {
            op = this.getPlace(p.getName());
            if (op.isPresent()) {
                return null;
            }
        } else {
            op = this.getPlace(p.getId());
            if (op.isPresent()) {
                op.get().setPicture(p.getPicture());
                op.get().setUsage(p.getUsage());
                op.get().setName(p.getName());
                p = op.get();
            } else {
                return null;
            }
        }
        for (Metier m : pSave) {
            om = metierRepository.findByName(m.getName());
            if (om.isPresent()){ //si le metier existe déjà on récupère son id
                toRem.add(m);
                toAdd.add(om.get());
            }
        }
        pSave.removeAll(toRem);
        pSave.addAll(toAdd);
        p.setMetiers(pSave);

        //INSERT OR UPDATE
        return this.save(p);
    }
}
