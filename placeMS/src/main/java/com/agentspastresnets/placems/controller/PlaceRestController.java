package com.agentspastresnets.placems.controller;

import com.agentspastresnets.placems.model.Place;
import com.agentspastresnets.placems.service.PlaceService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/places")
public class PlaceRestController {
    private PlaceService placeService;

    public PlaceRestController(PlaceService placeService) {
        this.placeService = placeService;
    }

    @GetMapping("/{id}")
    @ResponseBody
    public ResponseEntity getById(@PathVariable int id){
        Optional<Place> op = placeService.getPlace(id);
        if (op.isPresent()) {
            return new ResponseEntity<>(op.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Place with id " + id + " not found.", HttpStatus.NOT_FOUND);
        }
    }
    
    @GetMapping()
    @ResponseBody
    public ResponseEntity getAll(){
        return new ResponseEntity<>(placeService.getAll(), HttpStatus.OK);
    }
    
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity add(@RequestBody Place p){
        Place pl = placeService.insertOrUpdate(p, true);
        if (pl == null){
            return new ResponseEntity<>("Place to insert \"" + p.getName() + "\" already exists.", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(pl, HttpStatus.OK);
    }

    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity update(@RequestBody Place p, @PathVariable("id") int id){
        p.setId(id);
        Place pl = placeService.insertOrUpdate(p, false);
        if (pl == null){
            return new ResponseEntity<>("Place to update " + id + " does not exists.", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(pl, HttpStatus.OK);
    }
    
    @DeleteMapping("/{id}")
    @ResponseBody
    public ResponseEntity remove(@PathVariable int id) {
        placeService.remove(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(value = "/shuffle", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getShuffledPlaceId(Model model)
    {
        List<Place> places = new ArrayList<>();
        placeService.getAll().forEach(places::add);
        model.addAttribute("id", places.get((new Random()).nextInt(places.size())).getId());
        return new ResponseEntity<>(model, HttpStatus.OK);
    }
}
