package com.agentspastresnets.placems.model;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Place {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private int id;

    @Column(unique = true)
    private String name;
    private String picture;
    private int usage;

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            })
    @JoinTable(name = "place_metiers",
            joinColumns = { @JoinColumn(name = "place_id") },
            inverseJoinColumns = { @JoinColumn(name = "metier_id") })
    @Size(max=7)
    //@JsonManagedReference
    private Set<Metier> metiers = new HashSet<>();

    public Place() {
    }

    public Place(String name, String picture) {
        this.name = name;
        this.picture = picture;
        this.usage = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public int getUsage() {
        return usage;
    }

    public void setUsage(int usage) {
        this.usage = usage;
    }

    public Set<Metier> getMetiers() {
        return metiers;
    }

    public void setMetiers(Set<Metier> metiers) {
        this.metiers = metiers;
    }

    public int incUsage() {
        this.usage++;
        return this.usage;
    }
}
