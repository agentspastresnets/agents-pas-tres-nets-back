package com.agentspastresnets.placems;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PlaceMsApplication {
    public static void main(String[] args) {
        SpringApplication.run(PlaceMsApplication.class, args);
    }
}

