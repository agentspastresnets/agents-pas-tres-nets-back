package com.agentspastresnets.gamems.tools;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class ExternalRequests {

    public static boolean entityExists(String msUrl, int id) {
        try {
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<String> response
                    = restTemplate.getForEntity(msUrl + "/" + id, String.class);
            if (!response.getStatusCode().equals(HttpStatus.OK)) {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}
