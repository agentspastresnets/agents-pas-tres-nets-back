package com.agentspastresnets.gamems.dto;

public class NewGameDTO {
    private int creatorId;
    private double latitude;
    private double longitude;

    public NewGameDTO() {
    }

    public NewGameDTO(int creatorId, double latitude, double longitude) {
        this.creatorId = creatorId;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public int getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(int creatorId) {
        this.creatorId = creatorId;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

}
