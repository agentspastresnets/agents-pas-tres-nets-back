package com.agentspastresnets.gamems.dto;

public class TwoIdsDTO {
    private int gameId;
    private int entityId;

    public TwoIdsDTO(int gameId) {
        this.gameId = gameId;
    }

    public TwoIdsDTO() {
    }

    public int getGameId() {
        return gameId;
    }

    public void setGameId(int gameId) {
        this.gameId = gameId;
    }

    public int getEntityId() {
        return entityId;
    }

    public void setEntityId(int entityId) {
        this.entityId = entityId;
    }
}
