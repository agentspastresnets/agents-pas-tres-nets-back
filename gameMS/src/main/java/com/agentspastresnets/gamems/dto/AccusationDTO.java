package com.agentspastresnets.gamems.dto;

public class AccusationDTO {
    private int gameId;
    private int initierId;
    private int targetId;

    public AccusationDTO() {
    }

    public AccusationDTO(int gameId, int initierId, int targetId) {
        this.gameId = gameId;
        this.initierId = initierId;
        this.targetId = targetId;
    }

    public int getGameId() {
        return gameId;
    }

    public void setGameId(int gameId) {
        this.gameId = gameId;
    }

    public int getInitierId() {
        return initierId;
    }

    public void setInitierId(int initierId) {
        this.initierId = initierId;
    }

    public int getTargetId() {
        return targetId;
    }

    public void setTargetId(int targetId) {
        this.targetId = targetId;
    }
}
