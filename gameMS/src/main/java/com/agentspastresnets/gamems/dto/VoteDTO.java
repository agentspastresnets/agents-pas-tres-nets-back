package com.agentspastresnets.gamems.dto;

public class VoteDTO {
    private int accusationId;
    private int playerId;
    private boolean voteValue;

    public VoteDTO() {
    }

    public VoteDTO(int accusationId, int playerId, boolean voteValue) {
        this.accusationId = accusationId;
        this.playerId = playerId;
        this.voteValue = voteValue;
    }

    public int getAccusationId() {
        return accusationId;
    }

    public void setAccusationId(int accusationId) {
        this.accusationId = accusationId;
    }

    public int getPlayerId() {
        return playerId;
    }

    public void setPlayerId(int playerId) {
        this.playerId = playerId;
    }

    public boolean isVoteValue() {
        return voteValue;
    }

    public void setVoteValue(boolean voteValue) {
        this.voteValue = voteValue;
    }
}
