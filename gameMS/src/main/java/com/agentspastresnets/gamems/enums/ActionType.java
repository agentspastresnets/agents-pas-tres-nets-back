package com.agentspastresnets.gamems.enums;

public enum ActionType {
    ASK_QUESTION,
    ACCUSE
}
