package com.agentspastresnets.gamems.enums;

public enum GameStatus {
    IN_PROGRESS,
    PAUSED,
    WAIT_FOR_PLAYER,
    SPY_WON,
    AGENTS_WON,
    CANCELED
}
