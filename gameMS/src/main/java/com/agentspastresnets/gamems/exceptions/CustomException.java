package com.agentspastresnets.gamems.exceptions;

public class CustomException extends RuntimeException {
    private String message;

    public CustomException(String mess) {
        super(mess);
        this.message = mess;
    }

    @Override
    public String getMessage() {
        return this.message;
    }
}
