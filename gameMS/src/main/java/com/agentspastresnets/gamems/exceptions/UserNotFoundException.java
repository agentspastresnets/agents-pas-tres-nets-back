package com.agentspastresnets.gamems.exceptions;

public class UserNotFoundException extends CustomException {
    private int userId;

    public UserNotFoundException(int id) {
        super("User with id " + id + " does not exists according to user microservice.");
        userId = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
