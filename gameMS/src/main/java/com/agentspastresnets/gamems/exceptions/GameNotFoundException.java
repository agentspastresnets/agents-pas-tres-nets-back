package com.agentspastresnets.gamems.exceptions;

public class GameNotFoundException extends CustomException {
    private int gameId;
    public GameNotFoundException(int id) {
        super("Game with id " + id + " does not exists.");
        gameId = id;
    }

    public int getGameId() {
        return gameId;
    }

    public void setGameId(int gameId) {
        this.gameId = gameId;
    }
}
