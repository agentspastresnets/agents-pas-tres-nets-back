package com.agentspastresnets.gamems.repository;

import com.agentspastresnets.gamems.enums.GameStatus;
import com.agentspastresnets.gamems.model.Game;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GameRepository extends CrudRepository<Game, Integer> {
    Iterable<Game> findAllByGameStatus(GameStatus gs);
}
