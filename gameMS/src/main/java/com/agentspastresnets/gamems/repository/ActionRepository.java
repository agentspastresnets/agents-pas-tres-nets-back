package com.agentspastresnets.gamems.repository;

import com.agentspastresnets.gamems.model.Action;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ActionRepository extends CrudRepository<Action, Integer> {
    Iterable<Action> findByEndDateIsNull();
}
