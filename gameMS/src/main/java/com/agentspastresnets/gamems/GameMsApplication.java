package com.agentspastresnets.gamems;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GameMsApplication {

    public static void main(String[] args) {
        SpringApplication.run(GameMsApplication.class, args);
    }

}

