package com.agentspastresnets.gamems.controller;

import com.agentspastresnets.gamems.dto.AccusationDTO;
import com.agentspastresnets.gamems.dto.NewGameDTO;
import com.agentspastresnets.gamems.dto.TwoIdsDTO;
import com.agentspastresnets.gamems.dto.VoteDTO;
import com.agentspastresnets.gamems.exceptions.CustomException;
import com.agentspastresnets.gamems.model.Game;
import com.agentspastresnets.gamems.service.GameService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/game")
public class GameRestController {

    private GameService gameService;

    public GameRestController(GameService gameService) {
        this.gameService = gameService;
    }

    @GetMapping("/{id}")
    public ResponseEntity getById(@PathVariable("id") Integer id) {
        Optional<Game> og = gameService.getById(id);
        if (og.isPresent()) {
            return new ResponseEntity<>(og.get(), HttpStatus.OK);
        }
        return new ResponseEntity<>("Game with id " + id + " not found.", HttpStatus.NOT_FOUND);
    }

    @GetMapping
    public ResponseEntity getAll(){
        return new ResponseEntity<>(gameService.getAll(), HttpStatus.OK);
    }

    @PostMapping("/create")
    public ResponseEntity createDefaultGame(@RequestBody NewGameDTO dto) {
        try {
            Game g = gameService.createGameWithSocket(dto.getCreatorId(), dto.getLatitude(), dto.getLongitude());
            return new ResponseEntity<>(gameService.insertOrUpdate(g), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/available")
    public ResponseEntity getAllJoinableGames(){
        return new ResponseEntity<>(gameService.getAllWaitingForPlayersGames(), HttpStatus.OK);
    }

    @PostMapping(value = "/join", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity addPlayer(@RequestBody TwoIdsDTO infos){
        try {
            Game g = gameService.addPlayer(infos);
            return new ResponseEntity<>(gameService.insertOrUpdate(g), HttpStatus.OK);
        } catch (CustomException ce) {
            return new ResponseEntity<>(ce.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/start/{gameId}")
    public ResponseEntity startGame(@PathVariable("gameId") int gameId) {
        try {
            Game g = gameService.startGame(gameId);
            return new ResponseEntity<>(gameService.insertOrUpdate(g), HttpStatus.OK);
        } catch (CustomException ce) {
            return new ResponseEntity<>(ce.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/nextTurn/{gameId}")
    public ResponseEntity initiateNextTurn(@PathVariable("gameId") int gameId) {
        try {
            gameService.sendTargetablePlayersIds(gameId);
            return new ResponseEntity(HttpStatus.OK);
        } catch (CustomException ce) {
            return new ResponseEntity<>(ce.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "/newAskQuestion", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity nextQuestion(@RequestBody TwoIdsDTO dto) {
        try {
            gameService.newAskQuestionAction(dto.getGameId(), dto.getEntityId());
            return new ResponseEntity(HttpStatus.OK);
        } catch (CustomException ce) {
            return new ResponseEntity<>(ce.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "/spyTry", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity spyTry(@RequestBody TwoIdsDTO dto) {
        try {
            return new ResponseEntity<>(gameService.isPlace(dto.getGameId(), dto.getEntityId()), HttpStatus.OK);
        } catch (CustomException ce) {
            return new ResponseEntity<>(ce.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "/accuse", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity newAccusation(@RequestBody AccusationDTO dto) {
        try {
            Game g = gameService.newAccusation(dto.getGameId(), dto.getInitierId(), dto.getTargetId());
            return new ResponseEntity<>(g, HttpStatus.OK);
        } catch (CustomException ce) {
            return new ResponseEntity<>(ce.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "/vote", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity vote(@RequestBody VoteDTO dto) {
        try {
            Game g = gameService.vote(dto.getAccusationId(), dto.getPlayerId(), dto.isVoteValue());
            return new ResponseEntity<>(g, HttpStatus.OK);
        } catch (CustomException ce) {
            return new ResponseEntity<>(ce.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
