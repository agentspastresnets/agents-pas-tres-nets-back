package com.agentspastresnets.gamems.model;

import com.agentspastresnets.gamems.enums.GameStatus;
import org.hibernate.annotations.Check;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

@Entity
@Check(constraints = "game_status = 'IN_PROGRESS' or game_status = 'PAUSED' or game_status = 'WAIT_FOR_PLAYER' or game_status = 'SPY_WON' or game_status = 'AGENTS_WON' or game_status = 'CANCELED'")
public class Game {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private int id;

    @Enumerated(EnumType.STRING)
    private GameStatus gameStatus;

    @Column(name = "end_date", nullable = false)
    private OffsetDateTime endDate;

    @Column(name = "pause_date")
    private OffsetDateTime pauseDate;

    @Column(name = "creator_id", nullable = false)
    private int creatorId;

    @ElementCollection
    @CollectionTable(
            name="player_ids",
            joinColumns=@JoinColumn(name="game_id")
    )
    @Column(name = "player_ids_id")
    @Size(max=8)
    private List<Integer> playersId = new ArrayList<>();

    @Column(name = "spy_id", nullable = false)
    private int spyId;

    @OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy = "game")
    private List<Action> actions = new ArrayList<>();

    @Column(name = "place_id", nullable = false)
    private int placeId;

    private double latitude;

    private double longitude;

    public Game() {
    }

    public Game(GameStatus gameStatus, OffsetDateTime endDate, OffsetDateTime pauseDate, int creatorId, @Size(max = 8) List<Integer> playersId, int spyId, List<Action> actions, int placeId, double latitude, double longitude) {
        this.gameStatus = gameStatus;
        this.endDate = endDate;
        this.pauseDate = pauseDate;
        this.creatorId = creatorId;
        this.playersId = playersId;
        this.spyId = spyId;
        this.actions = actions;
        this.placeId = placeId;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Game(int creatorId, int placeId, double latitude, double longitude) {
        this.creatorId = creatorId;
        playersId.add(creatorId); //creator is the first player
        spyId = creatorId; //temporarly, the only one player is also the spy
        gameStatus = GameStatus.WAIT_FOR_PLAYER;
        endDate = OffsetDateTime.ofInstant(Instant.now().plusSeconds(60*20), ZoneId.systemDefault()); //default end date in 20 minutes
        pauseDate = null;
        actions = new ArrayList<>();
        this.placeId = placeId; //shuffle from PlaceMS called before this constructor
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public GameStatus getGameStatus() {
        return gameStatus;
    }

    public void setGameStatus(GameStatus gameStatus) {
        this.gameStatus = gameStatus;
    }

    public OffsetDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(OffsetDateTime endDate) {
        this.endDate = endDate;
    }

    public OffsetDateTime getPauseDate() {
        return pauseDate;
    }

    public void setPauseDate(OffsetDateTime pauseDate) {
        this.pauseDate = pauseDate;
    }

    public int getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(int creatorId) {
        this.creatorId = creatorId;
    }

    public List<Integer> getPlayersId() {
        return playersId;
    }

    public void setPlayersId(List<Integer> playersId) {
        this.playersId = playersId;
    }

    public int getSpyId() {
        return spyId;
    }

    public void setSpyId(int spyId) {
        this.spyId = spyId;
    }

    public List<Action> getActions() {
        return actions;
    }

    public void setActions(List<Action> actions) {
        this.actions = actions;
    }

    public int getPlaceId() {
        return placeId;
    }

    public void setPlaceId(int placeId) {
        this.placeId = placeId;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
