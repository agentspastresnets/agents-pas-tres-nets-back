package com.agentspastresnets.gamems.model.socketmessage;

import java.util.List;

public class ListContent<T> implements Content {
    private List<T> list;

    public ListContent() {
    }

    public ListContent(List<T> list) {
        this.list = list;
    }

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }
}
