package com.agentspastresnets.gamems.model.socketmessage;

public class GameMessage {
    private MessageType type;
    private Content content;

    public enum MessageType {
        JOIN,
        START,
        CONTENT_IS_LIST,
        ASK,
        ACCUSE,
        VOTE_EVOL,
        ACCUSATION_FAILED,
        VICTORY
    }

    public GameMessage(){

    }

    public GameMessage(MessageType type, Content content) {
        this.type = type;
        this.content = content;
    }

    public MessageType getType() {
        return type;
    }

    public void setType(MessageType type) {
        this.type = type;
    }

    public Content getContent() {
        return content;
    }

    public void setContent(Content content) {
        this.content = content;
    }

}
