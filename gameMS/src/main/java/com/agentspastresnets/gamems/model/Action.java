package com.agentspastresnets.gamems.model;

import com.agentspastresnets.gamems.enums.ActionType;
import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Action {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private int id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "game_id", nullable = false)
    @JsonBackReference
    private Game game;

    @Column(name = "initier_id", nullable = false)
    private int initierId;

    @Column(name = "target_id", nullable = false)
    private int targetId;

    @Enumerated(EnumType.STRING)
    private ActionType actionType;

    @Column(name = "start_date", nullable = false)
    private OffsetDateTime startDate;

    @Column(name = "end_date")
    private OffsetDateTime endDate;

    @OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy = "action")
    private List<Vote> votes = new ArrayList<>();

    public Action() {
    }

    public Action(Game game, int initierId, int targetId, ActionType actionType, OffsetDateTime startDate, OffsetDateTime endDate, List<Vote> votes) {
        this.game = game;
        this.initierId = initierId;
        this.targetId = targetId;
        this.actionType = actionType;
        this.startDate = startDate;
        this.endDate = endDate;
        this.votes = votes;
    }

    public Action(Game game, int initierId, int targetId, ActionType actionType) {
        this.game = game;
        this.initierId = initierId;
        this.targetId = targetId;
        this.actionType = actionType;
        this.startDate = OffsetDateTime.ofInstant(Instant.now(), ZoneId.systemDefault());
        this.endDate = null;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public int getInitierId() {
        return initierId;
    }

    public void setInitierId(int initierId) {
        this.initierId = initierId;
    }

    public int getTargetId() {
        return targetId;
    }

    public void setTargetId(int targetId) {
        this.targetId = targetId;
    }

    public ActionType getActionType() {
        return actionType;
    }

    public void setActionType(ActionType actionType) {
        this.actionType = actionType;
    }

    public OffsetDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(OffsetDateTime startDate) {
        this.startDate = startDate;
    }

    public OffsetDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(OffsetDateTime endDate) {
        this.endDate = endDate;
    }

    public List<Vote> getVotes() {
        return votes;
    }

    public void setVotes(List<Vote> votes) {
        this.votes = votes;
    }
}
