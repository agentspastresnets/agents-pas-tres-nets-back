package com.agentspastresnets.gamems.model.socketmessage;

import com.agentspastresnets.gamems.model.Game;

public class GameContent implements Content{
    private Game game;

    public GameContent () {}
    public GameContent(Game game) {
        this.game = game;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }
}
