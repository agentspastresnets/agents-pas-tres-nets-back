package com.agentspastresnets.gamems.service;

import com.agentspastresnets.gamems.dto.TwoIdsDTO;
import com.agentspastresnets.gamems.model.socketmessage.GameContent;
import com.agentspastresnets.gamems.model.socketmessage.GameMessage;
import com.agentspastresnets.gamems.model.socketmessage.ListContent;
import com.agentspastresnets.gamems.enums.ActionType;
import com.agentspastresnets.gamems.enums.GameStatus;
import com.agentspastresnets.gamems.exceptions.CustomException;
import com.agentspastresnets.gamems.exceptions.GameNotFoundException;
import com.agentspastresnets.gamems.exceptions.UserNotFoundException;
import com.agentspastresnets.gamems.model.Action;
import com.agentspastresnets.gamems.model.Game;
import com.agentspastresnets.gamems.model.Vote;
import com.agentspastresnets.gamems.repository.ActionRepository;
import com.agentspastresnets.gamems.repository.GameRepository;
import com.agentspastresnets.gamems.tools.ExternalRequests;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hibernate.proxy.HibernateProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

@Service
public class GameService {

    @Value("${placems.address}")
    public String placeMsUrl;

    @Value("${userms.address}")
    public String userMsUrl;

    private final GameRepository gameRepository;
    private final ActionRepository actionRepository;

    private static final String WARNINGMESSAGEGAME = "Game with id ";

    @Autowired
    private SimpMessagingTemplate template;

    @Autowired
    public GameService(GameRepository gameRepository, ActionRepository actionRepository) {
        this.gameRepository = gameRepository;
        this.actionRepository = actionRepository;
    }

    public Optional<Game> getById(Integer id) {
        return gameRepository.findById(id);
    }

    public Iterable<Game> getAll() {
        return gameRepository.findAll();
    }

    public Game insertOrUpdate(Game g){
        return gameRepository.save(g);
    }

    public List<Game> getAllWaitingForPlayersGames(){
        List<Game> gamesToUpdate = new ArrayList<>();
        List<Game> joinableGames = new ArrayList<>();
        for (Game g : gameRepository.findAllByGameStatus(GameStatus.WAIT_FOR_PLAYER)) {
            if (g.getEndDate().isBefore(OffsetDateTime.ofInstant(Instant.now(), ZoneId.systemDefault()))) {
                g.setGameStatus(GameStatus.CANCELED);
                gamesToUpdate.add(g);
            } else {
                joinableGames.add(g);
            }
        }
        gameRepository.saveAll(gamesToUpdate);
        return joinableGames;
    }

    private Game createDefaultGame(int id, double latitude, double longitude) throws IOException {
        if (!ExternalRequests.entityExists(userMsUrl, id)) {
            throw new UserNotFoundException(id);
        }
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> response
                = restTemplate.getForEntity(placeMsUrl + "/shuffle", String.class);
        if (!response.getStatusCode().equals(HttpStatus.OK)) {
            throw new CustomException("Requesting place/shuffle returned this status code : " + response.getStatusCode());
        }
        ObjectMapper mapper = new ObjectMapper();
        JsonNode root = mapper.readTree(response.getBody());
        JsonNode placeIdNode = root.path("id");

        return new Game(id, placeIdNode.intValue(), latitude, longitude);
    }

    public Game createGameWithSocket(int playerId, double latitude, double longitude) throws Exception {
        Game g = this.createDefaultGame(playerId, latitude, longitude);
        g = this.insertOrUpdate(g);
        return g;
    }

    public Game addPlayer(TwoIdsDTO infos) throws CustomException {
        if (!ExternalRequests.entityExists(userMsUrl, infos.getEntityId())) {
            throw new UserNotFoundException(infos.getEntityId());
        }
        Optional<Game> og = getById(infos.getGameId());
        if (!og.isPresent()) {
            throw new GameNotFoundException(infos.getGameId());
        } else if (!og.get().getGameStatus().equals(GameStatus.WAIT_FOR_PLAYER)){
            throw new CustomException(WARNINGMESSAGEGAME + infos.getGameId() + " is not waiting for new players");
        }
        List<Integer> playersIds= new ArrayList<>(og.get().getPlayersId());
        if (playersIds.size() >= 8) {
            throw new CustomException(WARNINGMESSAGEGAME + infos.getGameId() + " is full (8 players)");
        } else if (playersIds.contains(infos.getEntityId())) {
            throw new CustomException(WARNINGMESSAGEGAME + infos.getGameId() + " already has player with id " + infos.getEntityId());
        }
        og.get().getPlayersId().add(infos.getEntityId());

        //send through socket the info that a new player joined (or maybe refresh their game object?)
        sendGameContentMessage(og.get(), GameMessage.MessageType.JOIN);
        return og.get();
    }

    /* start the game :
    determine a spy and inform about it
    update end date
    determine the first player
    create corresponding action "talking"
     */
    public Game startGame(int gameId) throws CustomException {
        Optional<Game> og = getById(gameId);
        if (!og.isPresent()) {
            throw new GameNotFoundException(gameId);
        } else if(!og.get().getGameStatus().equals(GameStatus.WAIT_FOR_PLAYER)) {
            throw new CustomException("Game " + gameId + " is \"" + og.get().getGameStatus() + "\".");
        } else if (og.get().getPlayersId().size() < 3) {
            throw new CustomException("Game " + gameId + " have not enough players to start.");
        }
        Game g = og.get();
        g.setSpyId(g.getPlayersId().get((new Random()).nextInt(g.getPlayersId().size())));
        g.setEndDate(OffsetDateTime.ofInstant(Instant.now().plusSeconds(60*8), ZoneId.systemDefault()));
        g.setGameStatus(GameStatus.IN_PROGRESS);

        int firstPlayerId = g.getPlayersId().get((new Random()).nextInt(g.getPlayersId().size()));
        int firstTargetId = g.getPlayersId().get((new Random()).nextInt(g.getPlayersId().size()));
        while (firstPlayerId == firstTargetId) {
            firstTargetId = g.getPlayersId().get((new Random()).nextInt(g.getPlayersId().size()));
        }

        Action a = new Action(g, firstPlayerId, firstTargetId, ActionType.ASK_QUESTION);
        g.getActions().add(a);

        //inform by websockets about the current action.
        sendGameContentMessage(g, GameMessage.MessageType.START);
        return g;
    }

    /* nextTurn
    send to last target the list of all players except her and the last asker
     */
    public void sendTargetablePlayersIds(int gameId) throws CustomException {
        Game g = getgameObjectDuringGame(gameId);

        Action lastAction = getLastAskQuestionAction(gameId);
        List<Integer> result = new ArrayList<>(lastAction.getGame().getPlayersId());
        result.remove(new Integer(lastAction.getInitierId()));
        result.remove(new Integer(lastAction.getTargetId()));


        //send this list to precedent target using websockets
        sendListContentMessage(gameId, result);
    }

    private Game getgameObjectDuringGame(int gameId) {
        Optional<Game> og = getById(gameId);
        if (!og.isPresent()) {
            throw new GameNotFoundException(gameId);
        } else if (!og.get().getGameStatus().equals(GameStatus.IN_PROGRESS)){
            throw new CustomException(WARNINGMESSAGEGAME + gameId + " is not in progress.");
        }
        return og.get();
    }

    private Action getLastAskQuestionAction(int gameId) {
        List<Action> lastActions = new ArrayList<>();
        List<ActionType> actionTypes = new ArrayList<>();
        for (Action a : actionRepository.findByEndDateIsNull()) {
            if (a.getGame().getId() == gameId) {
                lastActions.add(a);
                actionTypes.add(a.getActionType());
            }
        }

        if (lastActions.size() != 1){
            if (actionTypes.contains(ActionType.ACCUSE)) {
                throw new CustomException("Cannot perform this service, because an action of type \"ACCUSE\" is currently happening in this game.");
            } else {
                throw new CustomException("Critical game logic error : several actions are opened for the same game.");
            }
        } else if(!lastActions.get(0).getActionType().equals(ActionType.ASK_QUESTION)) {
            throw new CustomException("Last action of the game isn't a \"ASK_QUESTION\" type.");
        }

        return lastActions.get(0);
    }

    /* newAskQuestion :
    find last action  and test if it is a askQuestion one
    update end date of last askQuestion action
     */
    public void newAskQuestionAction(int gameId, int targetId) {
        Game g = getgameObjectDuringGame(gameId);

        Action lastAction = getLastAskQuestionAction(gameId);

        if (targetId == lastAction.getTargetId()) {
            throw new CustomException("You can't ask a question to the same player that just have been asked one (" + targetId + ").");
        } else if (targetId == lastAction.getInitierId()) {
            throw new CustomException("You can't ask a question to the player who just asked one (" + targetId + ").");
        }

        if (g.getEndDate().isBefore(OffsetDateTime.ofInstant(Instant.now(), ZoneId.systemDefault()))) {
            terminateGame(g, GameStatus.SPY_WON);
            throw new CustomException("Game is finished, no new ask question began.");
        } else {
            lastAction.setEndDate(OffsetDateTime.ofInstant(Instant.now(), ZoneId.systemDefault()));
            Action newAskAction = new Action(g, lastAction.getTargetId(), targetId, ActionType.ASK_QUESTION);

            actionRepository.save(lastAction);
            actionRepository.save(newAskAction);
        }

        g = getgameObjectDuringGame(gameId);
        //send through websockets an update of the situation (!! normal case with a new askQuestion Action and special case where game is finished)
        sendGameContentMessage(g, GameMessage.MessageType.ASK);
    }

    /* Spy wins cases
    he find the place
        check if his proposition is right and inform everyone about the result
        terminate the game (close actions) and change status
        can't do it while accusation ?
        handle error if place does not exists
    time'up
        if end date of last action is higher than the game end date
        terminate the game (close actions) and change status
        inform everyone about the result
     */
    public Game isPlace(int gameId, int placeId) {
        if (!ExternalRequests.entityExists(placeMsUrl, placeId)) {
            throw new CustomException("According to place microservice, place " + placeId + " does not exists.");
        }

        Game g = this.getgameObjectDuringGame(gameId);
        GameStatus finalStatus;

        if (g.getPlaceId() == placeId) {
            //add different victory message
            finalStatus = GameStatus.SPY_WON;
        } else {
            //add different victory message
            finalStatus = GameStatus.AGENTS_WON;
        }
        terminateGame(g, finalStatus);

        // send victory infos through websockets
        //message is different depending on the case and explain each player role and why the game is finished
        //contains also an update of the game object ?
        return getById(gameId).get();
    }

    /**
     * set game final status, close actions
     * @param g
     * @param finalStatus
     */
    private void terminateGame(Game g, GameStatus finalStatus) {
        g.setGameStatus(finalStatus);

        List<Action> lastActions = new ArrayList<>();
        for (Action a : actionRepository.findByEndDateIsNull()) {
            if (a.getGame().getId() == g.getId()) {
                a.setEndDate(g.getEndDate());
                lastActions.add(a);
            }
        }

        this.insertOrUpdate(g);
        actionRepository.saveAll(lastActions);
        HibernateProxy hp = (HibernateProxy) this.getById(g.getId()).get();
        g = (Game) hp.getHibernateLazyInitializer().getImplementation();
        sendGameContentMessage(g, GameMessage.MessageType.VICTORY);
    }

    /* Accusation
    if game is in progress
    if lastAction isn't an accusation
    if initier hasn't created an accusation already
    create new accuse action object

    Vote
    add vote to accusation object
    if nb vote equals nb players - accusé
    possible end : all true -> test if target is spy
                    one or more false -> nothing happen, just send infos
     */
    public Game newAccusation(int gameId, int initierId, int targetId) {
        Game g = this.getgameObjectDuringGame(gameId);

        Action lastAction = this.getLastAskQuestionAction(gameId); // just for throw an error if there is already an accusation

        if (!g.getPlayersId().contains(new Integer(initierId))) {
            throw new CustomException("Player " + initierId + " is not in the game " + gameId + " .");
        }

        for (Action a : getAccusations(g)) {
            if (a.getInitierId() == initierId) {
                throw new CustomException("Player " + initierId + " already made an accusation.");
            }
        }

        if (!g.getPlayersId().contains(new Integer(targetId))) {
            throw new CustomException("Player " + targetId + " is not in the game " + gameId + " .");
        }

        Action newAccusation = new Action(g, initierId, targetId, ActionType.ACCUSE);
        newAccusation.getVotes().add(new Vote(newAccusation, initierId, true));
        g.getActions().add(newAccusation);

        g = this.insertOrUpdate(g);
        //inform through websocket about the accusation (with the whole game object ?)
        sendGameContentMessage(g, GameMessage.MessageType.ACCUSE);
        return g;
    }

    public Game vote(int accusationId, int playerId, boolean voteValue) {
        Optional<Action> oa = actionRepository.findById(accusationId);
        Action acc;
        if (oa.isPresent()) {
            acc = oa.get();
        } else {
            throw new CustomException("Accusation " + accusationId + " does not exist.");
        }

        if (!acc.getActionType().equals(ActionType.ACCUSE)){
            throw new CustomException("Action " + accusationId + " is not an accusation");
        } else if (acc.getEndDate() != null) {
            throw new CustomException("Accusation " + accusationId + " is closed.");
        } else if(acc.getTargetId() == playerId) {
            throw new CustomException("Target of the accusation (player " + playerId + ") can't vote.");
        }

        List<Integer> idWhoVoted = new ArrayList<>();
        for (Vote v: acc.getVotes()) {
            idWhoVoted.add(v.getPlayerId());
        }
        if (idWhoVoted.contains(new Integer(playerId))) {
            throw new CustomException("Player " + playerId + " already voted.");
        }

        Vote newVote = new Vote(acc, playerId, voteValue);
        acc.getVotes().add(newVote);

        GameMessage.MessageType messType = null;
        boolean gameEnded = false;
        HibernateProxy hp;
        Game g;
        //if vote is complete
        if (acc.getVotes().size() == acc.getGame().getPlayersId().size() - 1) {
            List<Boolean> votesValues = new ArrayList<>();
            for (Vote v : acc.getVotes()) {
                votesValues.add(v.isValue());
            }


            acc.setEndDate(OffsetDateTime.ofInstant(Instant.now(), ZoneId.systemDefault()));
            actionRepository.save(acc);
            if (votesValues.contains(new Boolean(false))) {
                //send through websockets the result of the vote
                messType = GameMessage.MessageType.ACCUSATION_FAILED;
            } else {
                hp = (HibernateProxy) this.getById(acc.getGame().getId()).get();
                g = (Game) hp.getHibernateLazyInitializer().getImplementation();
                if (acc.getTargetId() == acc.getGame().getSpyId()) {
                    //send through websockets the result of the vote
                    this.terminateGame(g, GameStatus.AGENTS_WON);
                } else {
                    //send through websockets the result of the vote
                    this.terminateGame(g, GameStatus.SPY_WON);
                }
                gameEnded = true;
            }
        } else {
            //inform through websocket that a new vote has been saved
            messType = GameMessage.MessageType.VOTE_EVOL;
        }

        //don't know why HibernateProxy (fetch lazy strategy) cause issue this time...
        hp = (HibernateProxy) this.getById(acc.getGame().getId()).get();
        g = (Game) hp.getHibernateLazyInitializer().getImplementation();
        if (!gameEnded){
            sendGameContentMessage(g, messType);
        }
        return g;
    }

    private List<Action> getAccusations(Game g) {
        List<Action> result = new ArrayList<>();
        for (Action a : g.getActions()) {
            if (a.getActionType().equals(ActionType.ACCUSE)) {
                result.add(a);
            }
        }
        return result;
    }

    private void sendGameContentMessage(Game g, GameMessage.MessageType mt) {
        this.template.convertAndSend("/topic/messages/" + g.getId(),
                new GameMessage(mt, new GameContent(g)));
    }
    private <T> void sendListContentMessage(int gameId, List<T> l) {
        this.template.convertAndSend("/topic/messages/" + gameId,
                new GameMessage(GameMessage.MessageType.CONTENT_IS_LIST, new ListContent<T>(l)));
    }
}
