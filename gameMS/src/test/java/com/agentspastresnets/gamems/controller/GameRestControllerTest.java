package com.agentspastresnets.gamems.controller;

import com.agentspastresnets.gamems.model.Game;
import com.agentspastresnets.gamems.service.GameService;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(value = GameRestController.class , secure = false)
public class GameRestControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private GameService gameService;

    @Test
    public void getById() throws Exception {
        Game mockGame = new Game(1,1,2,2);
        mockGame.setId(1);

        when(gameService.getById(Mockito.anyInt())).thenReturn(java.util.Optional.of(mockGame));

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/game/1")
                .contentType(MediaType.APPLICATION_JSON);

        String result = mockMvc.perform(requestBuilder).andReturn().getResponse().getContentAsString();
        String expected = "{\"id\":1,\"creatorId\":1,\"placeId\":1,\"latitude\":2,\"longitude\":2}";

        JSONAssert.assertEquals(expected, result, false);
    }

    @Test
    public void getAll() throws Exception {
        /*List<Game> mockGames = new ArrayList<>();
        mockGames.add(new Game(1,1,1,1));
        mockGames.add(new Game(2,2,2,2));

        when(gameService.getAll()).thenReturn(mockGames);*/

        this.mockMvc.perform(MockMvcRequestBuilders.get("/game")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void createDefaultGame() throws Exception {
        Game mockGame = new Game(1, 1, 2, 2);

        when(gameService.createGameWithSocket(Mockito.anyInt(), Mockito.anyDouble(), Mockito.anyDouble())).thenReturn(mockGame);

        this.mockMvc.perform(MockMvcRequestBuilders.post("/game/create")
                .content(asJsonString(mockGame))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void getAllJoinableGame() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.get("/game/available")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void addPlayer() throws Exception {
        Game mockGame = new Game(1, 1, 2, 2);

        when(gameService.addPlayer(Mockito.any())).thenReturn(mockGame);

        this.mockMvc.perform(MockMvcRequestBuilders.post("/game/join")
                .content(asJsonString(mockGame))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void spyTry() throws Exception {
        Game mockGame = new Game(1, 1, 2, 2);

        when(gameService.isPlace(Mockito.anyInt(), Mockito.anyInt())).thenReturn(mockGame);

        this.mockMvc.perform(MockMvcRequestBuilders.post("/game/spyTry")
                .content(asJsonString(mockGame))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void accuse() throws Exception {
        Game mockGame = new Game(1, 1, 2, 2);

        when(gameService.newAccusation(Mockito.anyInt(), Mockito.anyInt(), Mockito.anyInt())).thenReturn(mockGame);

        this.mockMvc.perform(MockMvcRequestBuilders.post("/game/accuse")
                .content(asJsonString(mockGame))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void vote() throws Exception {
        Game mockGame = new Game(1, 1, 2, 2);

        when(gameService.vote(Mockito.anyInt(), Mockito.anyInt(), Mockito.anyBoolean())).thenReturn(mockGame);

        this.mockMvc.perform(MockMvcRequestBuilders.post("/game/vote")
                .content(asJsonString(mockGame))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    // converts a Java object into JSON representation
    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}